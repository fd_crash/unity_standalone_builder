﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Unity_Standalone_Builder.Properties;

namespace Unity_Standalone_Builder {
    internal static class Builder {
        private static Form1.DebugLogDelegate _debugLog;

        internal static async Task StartBuild(Form1.DebugLogDelegate debugLog) {
            _debugLog = debugLog;
            if (Tools.CheckProjectPath(Settings.Default.ProjectPath)) {
                if (Tools.CheckUnityPath(Settings.Default.UnityPath)) {
                    Settings.Default.Save();
                    var name = Settings.Default.ProjectName;
                    if (!string.IsNullOrWhiteSpace(name)) {
                        var projectPath = Settings.Default.ProjectPath;
                        var buildBath = Path.Combine(projectPath, Resources.BuildsFolder,
                            DateTime.Now.ToString("yyyyMMdd"));
                        await StartMacBuild(name, buildBath, projectPath);
                        await StartLinuxBuild(name, buildBath, projectPath);
                        await StartWindowsBuild(name, buildBath, projectPath);
                    }
                } else {
                    //TODO create normal error messages
                    MessageBox.Show(@"Error");
                    //TODO create normal debug messages
                    await _debugLog(@"Error");
                }
            } else {
                //TODO create normal error messages
                MessageBox.Show(@"Error");
                //TODO create normal debug messages
                await _debugLog(@"Error");
            }
        }

        private static async Task StartMacBuild(string name, string buildBath, string projectPath) {
            var x86Path = Path.Combine(buildBath, Resources.Mac86BuildFolder);
            var x64Path = Path.Combine(buildBath, Resources.Mac64BuildFolder);
            var x86Name = Path.Combine(x86Path, name + Resources.MacExtension);
            var x64Name = Path.Combine(x64Path, name + Resources.MacExtension);

            if (Settings.Default.MacOSUniversal) {
                //TODO create normal debug messages
                await _debugLog(@"MacOSUniversal: building...");
                var tempBuildPath = Path.Combine(buildBath, Resources.MacBuildFolder);
                var tempBuildName = Path.Combine(tempBuildPath, name + Resources.MacExtension);
                var arguments = Tools.CreateBuildArguments(projectPath, Resources.BuildMacUniversal,
                    tempBuildName);

                await Tools.Build(arguments);
                //TODO create normal debug messages
                await _debugLog(@"MacOSUniversal: building completed.");

                Tools.WriteSteamAppId(tempBuildPath);
            } else {
                if (Settings.Default.MacOS86) {
                    //TODO create normal debug messages
                    await _debugLog(@"MacOS86: building...");
                    var arguments = Tools.CreateBuildArguments(projectPath, Resources.BuildMac86, x86Name);

                    await Tools.Build(arguments);
                    //TODO create normal debug messages
                    await _debugLog(@"MacOS86: building completed.");

                    if (!Settings.Default.MacOS64 || !Settings.Default.LinuxRestructure) {
                        Tools.WriteSteamAppId(x86Path);
                    }
                }
                if (Settings.Default.MacOS64) {
                    //TODO create normal debug messages
                    await _debugLog(@"MacOS64: building...");
                    var arguments = Tools.CreateBuildArguments(projectPath, Resources.BuildMac64, x64Name);

                    await Tools.Build(arguments);
                    //TODO create normal debug messages
                    await _debugLog(@"MacOS64: building completed.");

                    if (!Settings.Default.MacOS86 || !Settings.Default.LinuxRestructure) {
                        Tools.WriteSteamAppId(x64Path);
                    }
                }
            }
        }

        private static async Task StartLinuxBuild(string name, string buildBath, string projectPath) {
            var x86Path = Path.Combine(buildBath, Resources.Linux86BuildFolder);
            var x64Path = Path.Combine(buildBath, Resources.Linux64BuildFolder);
            var x86Name = Path.Combine(x86Path, name + Resources.LinuxExtension);
            var x64Name = Path.Combine(x64Path, name + Resources.Linux64Extension);

            if (Settings.Default.LinuxUniversal) {
                //TODO create normal debug messages
                await _debugLog(@"LinuxUniversal: building...");
                var tempBuildPath = Path.Combine(buildBath, Resources.LinuxBuildFolder);
                var tempBuildName = Path.Combine(tempBuildPath, name + Resources.LinuxExtension);
                var arguments = Tools.CreateBuildArguments(projectPath, Resources.BuildLinuxUniversal,
                    tempBuildName);

                await Tools.Build(arguments);
                //TODO create normal debug messages
                await _debugLog(@"LinuxUniversal: building completed.");

                Tools.WriteSteamAppId(tempBuildPath);
            } else {
                if (Settings.Default.Linux86) {
                    //TODO create normal debug messages
                    await _debugLog(@"Linux86: building...");
                    var arguments = Tools.CreateBuildArguments(projectPath, Resources.BuildLinux86, x86Name);

                    await Tools.Build(arguments);
                    //TODO create normal debug messages
                    await _debugLog(@"Linux86: building completed.");

                    if (!Settings.Default.Linux64 || !Settings.Default.LinuxRestructure) {
                        Tools.WriteSteamAppId(x86Path);
                    }
                }
                if (Settings.Default.Linux64) {
                    //TODO create normal debug messages
                    await _debugLog(@"Linux64: building...");
                    var arguments = Tools.CreateBuildArguments(projectPath, Resources.BuildLinux64, x64Name);

                    await Tools.Build(arguments);
                    //TODO create normal debug messages
                    await _debugLog(@"Linux86: building completed.");

                    if (!Settings.Default.Linux86 || !Settings.Default.LinuxRestructure) {
                        Tools.WriteSteamAppId(x64Path);
                    }
                }
            }
        }

        private static async Task StartWindowsBuild(string name, string buildBath, string projectPath) {
            var x86Path = Path.Combine(buildBath, Resources.Windows86BuildFolder);
            var x64Path = Path.Combine(buildBath, Resources.Windows64BuildFolder);
            var x86Name = Path.Combine(x86Path, name + Resources.WindowsExtension);
            var x64Name = Path.Combine(x64Path, name + Resources.WindowsExtension);

            if (Settings.Default.Windows86) {
                //TODO create normal debug messages
                await _debugLog(@"Windows86: building...");
                var arguments = Tools.CreateBuildArguments(projectPath, Resources.BuildWindows86, x86Name);

                await Tools.Build(arguments);

                foreach (var fInfo in new DirectoryInfo(x86Path).GetFiles()) {
                    if (fInfo.Name.EndsWith(".pdb")) {
                        fInfo.Delete();
                    }
                }

                //TODO create normal debug messages
                await _debugLog(@"Windows86: building completed.");

                if (!Settings.Default.Windows64 || !Settings.Default.WindowsRestructure) {
                    Tools.WriteSteamAppId(x86Path);
                }
            }
            if (Settings.Default.Windows64) {
                //TODO create normal debug messages
                await _debugLog(@"Windows64: building...");
                var arguments = Tools.CreateBuildArguments(projectPath, Resources.BuildWindows64, x64Name);

                await Tools.Build(arguments);

                foreach (var fInfo in new DirectoryInfo(x64Path).GetFiles()) {
                    if (fInfo.Name.EndsWith(".pdb")) {
                        fInfo.Delete();
                    }
                }
                //TODO create normal debug messages
                await _debugLog(@"Windows64: building completed.");

                if (!Settings.Default.Windows86 || !Settings.Default.WindowsRestructure) {
                    Tools.WriteSteamAppId(x64Path);
                }
            }
            if (Settings.Default.Windows86 && Settings.Default.Windows64 &&
                Settings.Default.WindowsRestructure) {
                //TODO create normal debug messages
                await _debugLog(@"Windows: folders restructuring...");
                var tempBuildPath = Path.Combine(buildBath, Resources.WindowsBuildFolder);
                if (!Directory.Exists(tempBuildPath)) {
                    Directory.CreateDirectory(tempBuildPath);
                }
                if (Directory.Exists(x64Path) && Directory.Exists(tempBuildPath)) {
                    var directories = new DirectoryInfo(x64Path).GetDirectories(name + Resources.DataFolder);
                    if (directories.Any()) {
                        var dataName = directories.First().Name;

                        var fromPath = Path.Combine(x64Path, dataName);
                        var destPath = Path.Combine(tempBuildPath, dataName);
                        if (Directory.Exists(fromPath)) {
                            Directory.Move(fromPath, destPath);
                            if (!Directory.Exists(fromPath)) {
                                Directory.CreateDirectory(fromPath);
                            }
                            if (Directory.Exists(Path.Combine(destPath, Resources.ManagedFolder)) &&
                                !Directory.Exists(Path.Combine(fromPath, Resources.ManagedFolder))) {
                                Directory.Move(Path.Combine(destPath, Resources.ManagedFolder),
                                    Path.Combine(fromPath, Resources.ManagedFolder));
                            }
                            if (Directory.Exists(Path.Combine(destPath, Resources.MonoFolder)) &&
                                !Directory.Exists(Path.Combine(fromPath, Resources.MonoFolder))) {
                                Directory.Move(Path.Combine(destPath, Resources.MonoFolder),
                                    Path.Combine(fromPath, Resources.MonoFolder));
                            }
                            if (Directory.Exists(Path.Combine(destPath, Resources.PluginsFolder)) &&
                                !Directory.Exists(Path.Combine(fromPath, Resources.PluginsFolder))) {
                                Directory.Move(Path.Combine(destPath, Resources.PluginsFolder),
                                    Path.Combine(fromPath, Resources.PluginsFolder));
                            }
                        }

                        fromPath = Path.Combine(x86Path, dataName);
                        destPath = Path.Combine(x86Path, dataName + Resources.TempFolder);

                        if (!Directory.Exists(destPath)) {
                            Directory.CreateDirectory(destPath);
                        }
                        if (Directory.Exists(Path.Combine(fromPath, Resources.ManagedFolder)) &&
                            !Directory.Exists(Path.Combine(destPath, Resources.ManagedFolder))) {
                            Directory.Move(Path.Combine(fromPath, Resources.ManagedFolder),
                                Path.Combine(destPath, Resources.ManagedFolder));
                        }
                        if (Directory.Exists(Path.Combine(fromPath, Resources.MonoFolder)) &&
                            !Directory.Exists(Path.Combine(destPath, Resources.MonoFolder))) {
                            Directory.Move(Path.Combine(fromPath, Resources.MonoFolder),
                                Path.Combine(destPath, Resources.MonoFolder));
                        }
                        if (Directory.Exists(Path.Combine(fromPath, Resources.PluginsFolder)) &&
                            !Directory.Exists(Path.Combine(destPath, Resources.PluginsFolder))) {
                            Directory.Move(Path.Combine(fromPath, Resources.PluginsFolder),
                                Path.Combine(destPath, Resources.PluginsFolder));
                        }

                        if (Directory.Exists(fromPath)) {
                            Directory.Delete(fromPath, true);
                        }
                        if (Directory.Exists(destPath) && !Directory.Exists(fromPath)) {
                            Directory.Move(destPath, fromPath);
                        }
                    }
                }
                //TODO create normal debug messages
                await _debugLog(@"Windows: folders restructure completed.");

                Tools.WriteSteamAppId(tempBuildPath);
            }
        }
    }
}
