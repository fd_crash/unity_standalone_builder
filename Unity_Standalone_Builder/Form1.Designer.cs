﻿namespace Unity_Standalone_Builder {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.tbProjectPath = new System.Windows.Forms.TextBox();
            this.bOpenProjectFolder = new System.Windows.Forms.Button();
            this.lProjectStatus = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbWindows64 = new System.Windows.Forms.CheckBox();
            this.cbWindows86 = new System.Windows.Forms.CheckBox();
            this.cbWindowsUniversal = new System.Windows.Forms.CheckBox();
            this.cbLinux64 = new System.Windows.Forms.CheckBox();
            this.cbLinux86 = new System.Windows.Forms.CheckBox();
            this.cbLinuxUniversal = new System.Windows.Forms.CheckBox();
            this.cbMac64 = new System.Windows.Forms.CheckBox();
            this.cbMac86 = new System.Windows.Forms.CheckBox();
            this.cbMacUniversal = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.cbMacRestructure = new System.Windows.Forms.CheckBox();
            this.cbLinuxRestructure = new System.Windows.Forms.CheckBox();
            this.cbWindowsRestructure = new System.Windows.Forms.CheckBox();
            this.bStart = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.lUnityStatus = new System.Windows.Forms.Label();
            this.tbUnityPath = new System.Windows.Forms.TextBox();
            this.bOpenUnityFolder = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tbDebugLog = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbProjectName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbSteamAppId = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbProjectPath
            // 
            this.tbProjectPath.Location = new System.Drawing.Point(61, 12);
            this.tbProjectPath.Name = "tbProjectPath";
            this.tbProjectPath.ReadOnly = true;
            this.tbProjectPath.Size = new System.Drawing.Size(242, 20);
            this.tbProjectPath.TabIndex = 0;
            this.tbProjectPath.TabStop = false;
            // 
            // bOpenProjectFolder
            // 
            this.bOpenProjectFolder.Location = new System.Drawing.Point(309, 10);
            this.bOpenProjectFolder.Name = "bOpenProjectFolder";
            this.bOpenProjectFolder.Size = new System.Drawing.Size(29, 23);
            this.bOpenProjectFolder.TabIndex = 1;
            this.bOpenProjectFolder.Text = "...";
            this.bOpenProjectFolder.UseVisualStyleBackColor = true;
            this.bOpenProjectFolder.Click += new System.EventHandler(this.bOpenProjectFolder_Click);
            // 
            // lProjectStatus
            // 
            this.lProjectStatus.AutoSize = true;
            this.lProjectStatus.Location = new System.Drawing.Point(12, 15);
            this.lProjectStatus.Name = "lProjectStatus";
            this.lProjectStatus.Size = new System.Drawing.Size(43, 13);
            this.lProjectStatus.TabIndex = 2;
            this.lProjectStatus.Text = "Project:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Windows";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Linux";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "MacOS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "x64";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(95, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "x86";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(123, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Universal";
            // 
            // cbWindows64
            // 
            this.cbWindows64.AutoSize = true;
            this.cbWindows64.Location = new System.Drawing.Point(57, 23);
            this.cbWindows64.Name = "cbWindows64";
            this.cbWindows64.Size = new System.Drawing.Size(15, 14);
            this.cbWindows64.TabIndex = 4;
            this.cbWindows64.Tag = "Windows";
            this.cbWindows64.UseVisualStyleBackColor = true;
            this.cbWindows64.CheckedChanged += new System.EventHandler(this.cbChangePlatform_CheckedChanged);
            // 
            // cbWindows86
            // 
            this.cbWindows86.AutoSize = true;
            this.cbWindows86.Location = new System.Drawing.Point(98, 23);
            this.cbWindows86.Name = "cbWindows86";
            this.cbWindows86.Size = new System.Drawing.Size(15, 14);
            this.cbWindows86.TabIndex = 5;
            this.cbWindows86.Tag = "Windows";
            this.cbWindows86.UseVisualStyleBackColor = true;
            this.cbWindows86.CheckedChanged += new System.EventHandler(this.cbChangePlatform_CheckedChanged);
            // 
            // cbWindowsUniversal
            // 
            this.cbWindowsUniversal.AutoSize = true;
            this.cbWindowsUniversal.Enabled = false;
            this.cbWindowsUniversal.Location = new System.Drawing.Point(140, 23);
            this.cbWindowsUniversal.Name = "cbWindowsUniversal";
            this.cbWindowsUniversal.Size = new System.Drawing.Size(15, 14);
            this.cbWindowsUniversal.TabIndex = 6;
            this.cbWindowsUniversal.Tag = "Windows";
            this.cbWindowsUniversal.UseVisualStyleBackColor = true;
            this.cbWindowsUniversal.CheckedChanged += new System.EventHandler(this.cbChangePlatform_CheckedChanged);
            // 
            // cbLinux64
            // 
            this.cbLinux64.AutoSize = true;
            this.cbLinux64.Location = new System.Drawing.Point(57, 49);
            this.cbLinux64.Name = "cbLinux64";
            this.cbLinux64.Size = new System.Drawing.Size(15, 14);
            this.cbLinux64.TabIndex = 9;
            this.cbLinux64.Tag = "Linux";
            this.cbLinux64.UseVisualStyleBackColor = true;
            this.cbLinux64.CheckedChanged += new System.EventHandler(this.cbChangePlatform_CheckedChanged);
            // 
            // cbLinux86
            // 
            this.cbLinux86.AutoSize = true;
            this.cbLinux86.Location = new System.Drawing.Point(98, 49);
            this.cbLinux86.Name = "cbLinux86";
            this.cbLinux86.Size = new System.Drawing.Size(15, 14);
            this.cbLinux86.TabIndex = 10;
            this.cbLinux86.Tag = "Linux";
            this.cbLinux86.UseVisualStyleBackColor = true;
            this.cbLinux86.CheckedChanged += new System.EventHandler(this.cbChangePlatform_CheckedChanged);
            // 
            // cbLinuxUniversal
            // 
            this.cbLinuxUniversal.AutoSize = true;
            this.cbLinuxUniversal.Location = new System.Drawing.Point(140, 49);
            this.cbLinuxUniversal.Name = "cbLinuxUniversal";
            this.cbLinuxUniversal.Size = new System.Drawing.Size(15, 14);
            this.cbLinuxUniversal.TabIndex = 11;
            this.cbLinuxUniversal.Tag = "Linux";
            this.cbLinuxUniversal.UseVisualStyleBackColor = true;
            this.cbLinuxUniversal.CheckedChanged += new System.EventHandler(this.cbChangePlatform_CheckedChanged);
            // 
            // cbMac64
            // 
            this.cbMac64.AutoSize = true;
            this.cbMac64.Location = new System.Drawing.Point(57, 75);
            this.cbMac64.Name = "cbMac64";
            this.cbMac64.Size = new System.Drawing.Size(15, 14);
            this.cbMac64.TabIndex = 14;
            this.cbMac64.Tag = "Mac";
            this.cbMac64.UseVisualStyleBackColor = true;
            this.cbMac64.CheckedChanged += new System.EventHandler(this.cbChangePlatform_CheckedChanged);
            // 
            // cbMac86
            // 
            this.cbMac86.AutoSize = true;
            this.cbMac86.Location = new System.Drawing.Point(98, 75);
            this.cbMac86.Name = "cbMac86";
            this.cbMac86.Size = new System.Drawing.Size(15, 14);
            this.cbMac86.TabIndex = 15;
            this.cbMac86.Tag = "Mac";
            this.cbMac86.UseVisualStyleBackColor = true;
            this.cbMac86.CheckedChanged += new System.EventHandler(this.cbChangePlatform_CheckedChanged);
            // 
            // cbMacUniversal
            // 
            this.cbMacUniversal.AutoSize = true;
            this.cbMacUniversal.Location = new System.Drawing.Point(140, 75);
            this.cbMacUniversal.Name = "cbMacUniversal";
            this.cbMacUniversal.Size = new System.Drawing.Size(15, 14);
            this.cbMacUniversal.TabIndex = 16;
            this.cbMacUniversal.Tag = "Mac";
            this.cbMacUniversal.UseVisualStyleBackColor = true;
            this.cbMacUniversal.CheckedChanged += new System.EventHandler(this.cbChangePlatform_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.cbMacRestructure);
            this.panel1.Controls.Add(this.cbMacUniversal);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cbLinuxRestructure);
            this.panel1.Controls.Add(this.cbLinuxUniversal);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cbWindowsRestructure);
            this.panel1.Controls.Add(this.cbWindowsUniversal);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cbMac86);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.cbLinux86);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.cbWindows86);
            this.panel1.Controls.Add(this.cbWindows64);
            this.panel1.Controls.Add(this.cbMac64);
            this.panel1.Controls.Add(this.cbLinux64);
            this.panel1.Location = new System.Drawing.Point(12, 93);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(245, 96);
            this.panel1.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(180, 4);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Restructure";
            // 
            // cbMacRestructure
            // 
            this.cbMacRestructure.AutoSize = true;
            this.cbMacRestructure.Enabled = false;
            this.cbMacRestructure.Location = new System.Drawing.Point(202, 75);
            this.cbMacRestructure.Name = "cbMacRestructure";
            this.cbMacRestructure.Size = new System.Drawing.Size(15, 14);
            this.cbMacRestructure.TabIndex = 17;
            this.cbMacRestructure.Tag = "Mac";
            this.cbMacRestructure.UseVisualStyleBackColor = true;
            this.cbMacRestructure.CheckedChanged += new System.EventHandler(this.cbChangePlatform_CheckedChanged);
            // 
            // cbLinuxRestructure
            // 
            this.cbLinuxRestructure.AutoSize = true;
            this.cbLinuxRestructure.Enabled = false;
            this.cbLinuxRestructure.Location = new System.Drawing.Point(202, 49);
            this.cbLinuxRestructure.Name = "cbLinuxRestructure";
            this.cbLinuxRestructure.Size = new System.Drawing.Size(15, 14);
            this.cbLinuxRestructure.TabIndex = 12;
            this.cbLinuxRestructure.Tag = "Linux";
            this.cbLinuxRestructure.UseVisualStyleBackColor = true;
            this.cbLinuxRestructure.CheckedChanged += new System.EventHandler(this.cbChangePlatform_CheckedChanged);
            // 
            // cbWindowsRestructure
            // 
            this.cbWindowsRestructure.AutoSize = true;
            this.cbWindowsRestructure.Location = new System.Drawing.Point(202, 23);
            this.cbWindowsRestructure.Name = "cbWindowsRestructure";
            this.cbWindowsRestructure.Size = new System.Drawing.Size(15, 14);
            this.cbWindowsRestructure.TabIndex = 7;
            this.cbWindowsRestructure.Tag = "Windows";
            this.cbWindowsRestructure.UseVisualStyleBackColor = true;
            this.cbWindowsRestructure.CheckedChanged += new System.EventHandler(this.cbChangePlatform_CheckedChanged);
            // 
            // bStart
            // 
            this.bStart.Location = new System.Drawing.Point(263, 166);
            this.bStart.Name = "bStart";
            this.bStart.Size = new System.Drawing.Size(75, 23);
            this.bStart.TabIndex = 20;
            this.bStart.Text = "Start";
            this.bStart.UseVisualStyleBackColor = true;
            this.bStart.Click += new System.EventHandler(this.bStart_Click);
            // 
            // lUnityStatus
            // 
            this.lUnityStatus.AutoSize = true;
            this.lUnityStatus.Location = new System.Drawing.Point(12, 44);
            this.lUnityStatus.Name = "lUnityStatus";
            this.lUnityStatus.Size = new System.Drawing.Size(34, 13);
            this.lUnityStatus.TabIndex = 2;
            this.lUnityStatus.Text = "Unity:";
            // 
            // tbUnityPath
            // 
            this.tbUnityPath.Location = new System.Drawing.Point(61, 41);
            this.tbUnityPath.Name = "tbUnityPath";
            this.tbUnityPath.ReadOnly = true;
            this.tbUnityPath.Size = new System.Drawing.Size(242, 20);
            this.tbUnityPath.TabIndex = 0;
            this.tbUnityPath.TabStop = false;
            // 
            // bOpenUnityFolder
            // 
            this.bOpenUnityFolder.Location = new System.Drawing.Point(309, 39);
            this.bOpenUnityFolder.Name = "bOpenUnityFolder";
            this.bOpenUnityFolder.Size = new System.Drawing.Size(29, 23);
            this.bOpenUnityFolder.TabIndex = 2;
            this.bOpenUnityFolder.Text = "...";
            this.bOpenUnityFolder.UseVisualStyleBackColor = true;
            this.bOpenUnityFolder.Click += new System.EventHandler(this.bOpenUnityFolder_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "Unity.exe";
            this.openFileDialog1.Filter = "Unity.exe|Unity.exe|All files (*.*)|*.*";
            this.openFileDialog1.InitialDirectory = "%PROGRAMFILES%";
            // 
            // tbDebugLog
            // 
            this.tbDebugLog.BackColor = System.Drawing.Color.Navy;
            this.tbDebugLog.ForeColor = System.Drawing.SystemColors.Window;
            this.tbDebugLog.Location = new System.Drawing.Point(12, 195);
            this.tbDebugLog.Multiline = true;
            this.tbDebugLog.Name = "tbDebugLog";
            this.tbDebugLog.ReadOnly = true;
            this.tbDebugLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbDebugLog.Size = new System.Drawing.Size(326, 175);
            this.tbDebugLog.TabIndex = 21;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 70);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "Name";
            // 
            // tbProjectName
            // 
            this.tbProjectName.Location = new System.Drawing.Point(61, 67);
            this.tbProjectName.Name = "tbProjectName";
            this.tbProjectName.Size = new System.Drawing.Size(100, 20);
            this.tbProjectName.TabIndex = 23;
            this.tbProjectName.TextChanged += new System.EventHandler(this.tbProjectName_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(167, 70);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "SteamAppId";
            // 
            // tbSteamAppId
            // 
            this.tbSteamAppId.Location = new System.Drawing.Point(238, 67);
            this.tbSteamAppId.Name = "tbSteamAppId";
            this.tbSteamAppId.Size = new System.Drawing.Size(100, 20);
            this.tbSteamAppId.TabIndex = 23;
            this.tbSteamAppId.TextChanged += new System.EventHandler(this.tbSteamAppId_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 379);
            this.Controls.Add(this.tbSteamAppId);
            this.Controls.Add(this.tbProjectName);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbDebugLog);
            this.Controls.Add(this.bStart);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lUnityStatus);
            this.Controls.Add(this.lProjectStatus);
            this.Controls.Add(this.bOpenUnityFolder);
            this.Controls.Add(this.bOpenProjectFolder);
            this.Controls.Add(this.tbUnityPath);
            this.Controls.Add(this.tbProjectPath);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Unity Standalone Builder";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbProjectPath;
        private System.Windows.Forms.Button bOpenProjectFolder;
        private System.Windows.Forms.Label lProjectStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox cbWindows64;
        private System.Windows.Forms.CheckBox cbWindows86;
        private System.Windows.Forms.CheckBox cbWindowsUniversal;
        private System.Windows.Forms.CheckBox cbLinux64;
        private System.Windows.Forms.CheckBox cbLinux86;
        private System.Windows.Forms.CheckBox cbLinuxUniversal;
        private System.Windows.Forms.CheckBox cbMac64;
        private System.Windows.Forms.CheckBox cbMac86;
        private System.Windows.Forms.CheckBox cbMacUniversal;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox cbMacRestructure;
        private System.Windows.Forms.CheckBox cbLinuxRestructure;
        private System.Windows.Forms.CheckBox cbWindowsRestructure;
        private System.Windows.Forms.Button bStart;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label lUnityStatus;
        private System.Windows.Forms.TextBox tbUnityPath;
        private System.Windows.Forms.Button bOpenUnityFolder;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox tbDebugLog;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbProjectName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbSteamAppId;
    }
}

