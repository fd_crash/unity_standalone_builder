﻿using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Unity_Standalone_Builder.Properties;

namespace Unity_Standalone_Builder {
    internal static class Tools {
        internal static async Task Build(string arguments) {
            var process = new Process {
                StartInfo = {
                    FileName = Properties.Settings.Default.UnityPath,
                    Arguments = arguments,
                    ErrorDialog = true,
                    WindowStyle = ProcessWindowStyle.Minimized
                }
            };
            process.Start();
            await WaitForExitAsync(process);
        }

        public static Task WaitForExitAsync(this Process process,
            CancellationToken cancellationToken = default(CancellationToken)) {
            var tcs = new TaskCompletionSource<object>();
            process.EnableRaisingEvents = true;
            process.Exited += (sender, args) => tcs.TrySetResult(null);
            if (cancellationToken != default(CancellationToken)) {
                cancellationToken.Register(tcs.SetCanceled);
            }

            return tcs.Task;
        }

        internal static bool CheckUnityPath(string path) {
            return File.Exists(path) && path.EndsWith(Resources.UnityFile);
        }

        internal static bool CheckProjectPath(string path) {
            if (Directory.Exists(path)) {
                var directory = new DirectoryInfo(path);

                if (directory.GetDirectories(Resources.AssetsFolder).Length > 0 &&
                    directory.GetDirectories(Resources.ProjectSettingsFolder).Length > 0 &&
                    directory.GetDirectories(Resources.LibraryFolder).Length > 0) {
                    return true;
                }
            }

            return false;
        }

        internal static string CreateBuildArguments(string projectPath, string version, string outputName) {
            var result = Resources.BuildString.Replace("[0]", projectPath)
                .Replace("[1]", version)
                .Replace("[2]", outputName);

            return result;
        }

        internal static void WriteSteamAppId(string path) {
            if (Settings.Default.SteamAppId > 0) {
                File.WriteAllText(Path.Combine(path, Resources.SteamAppIdFile), Settings.Default.SteamAppId.ToString());
            }
        }
    }
}
