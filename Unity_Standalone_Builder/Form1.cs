﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Forms;
using Unity_Standalone_Builder.Properties;

namespace Unity_Standalone_Builder {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e) {
            if (Tools.CheckUnityPath(Settings.Default.UnityPath)) {
                tbUnityPath.Text = Settings.Default.UnityPath;
            }

            if (Tools.CheckProjectPath(Settings.Default.ProjectPath)) {
                tbProjectPath.Text = Settings.Default.ProjectPath;
            }

            cbWindows64.Checked = Settings.Default.Windows64;
            cbWindows86.Checked = Settings.Default.Windows86;
            cbWindowsRestructure.Checked = Settings.Default.WindowsRestructure;

            cbLinux64.Checked = Settings.Default.Linux64;
            cbLinux86.Checked = Settings.Default.Linux86;
            cbLinuxUniversal.Checked = Settings.Default.LinuxUniversal;
            cbLinuxRestructure.Checked = Settings.Default.LinuxRestructure;

            cbMac64.Checked = Settings.Default.MacOS64;
            cbMac86.Checked = Settings.Default.MacOS86;
            cbMacUniversal.Checked = Settings.Default.MacOSUniversal;
            cbMacRestructure.Checked = Settings.Default.MacOSRestructure;

            tbSteamAppId.Text = Settings.Default.SteamAppId.ToString();
            tbProjectName.Text = Settings.Default.ProjectName;

            CheckCheckBoxes();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e) {
            Settings.Default.Save();
        }

        private void cbChangePlatform_CheckedChanged(object sender, EventArgs e) {
            var checkBox = (CheckBox) sender;
            switch ((string) checkBox.Tag) {
                case Platforms.Windows:
                    if (checkBox.Name.Contains(Resources.X86Version)) {
                        Settings.Default.Windows86 = checkBox.Checked;
                    } else if (checkBox.Name.Contains(Resources.X64Version)) {
                        Settings.Default.Windows64 = checkBox.Checked;
                    } else if (checkBox.Name.Contains(Resources.Restructure)) {
                        Settings.Default.WindowsRestructure = checkBox.Checked;
                    }
                    break;
                case Platforms.Linux:
                    if (checkBox.Name.Contains(Resources.UniversalVersion)) {
                        Settings.Default.LinuxUniversal = checkBox.Checked;
                    } else if (checkBox.Name.Contains(Resources.X86Version)) {
                        Settings.Default.Linux86 = checkBox.Checked;
                    } else if (checkBox.Name.Contains(Resources.X64Version)) {
                        Settings.Default.Linux64 = checkBox.Checked;
                    }
                    break;
                case Platforms.MacOs:
                    if (checkBox.Name.Contains(Resources.UniversalVersion)) {
                        Settings.Default.MacOSUniversal = checkBox.Checked;
                    } else if (checkBox.Name.Contains(Resources.X86Version)) {
                        Settings.Default.MacOS86 = checkBox.Checked;
                    } else if (checkBox.Name.Contains(Resources.X64Version)) {
                        Settings.Default.MacOS64 = checkBox.Checked;
                    }
                    break;
            }
            CheckCheckBoxes();
        }

        private void bOpenProjectFolder_Click(object sender, EventArgs e) {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK) {
                var path = folderBrowserDialog1.SelectedPath;
                if (Tools.CheckProjectPath(path)) {
                    tbProjectPath.Text = path;
                    Settings.Default.ProjectPath = path;
                } else {
                    //TODO create normal error messages
                    MessageBox.Show(@"Error");
                }
            }
        }

        private void bOpenUnityFolder_Click(object sender, EventArgs e) {
            if (openFileDialog1.ShowDialog() == DialogResult.OK) {
                var path = openFileDialog1.FileName;
                if (Tools.CheckUnityPath(path)) {
                    tbUnityPath.Text = path;
                    Settings.Default.UnityPath = path;
                } else {
                    //TODO create normal error messages
                    MessageBox.Show(@"Error");
                }
            }
        }

        private void tbSteamAppId_TextChanged(object sender, EventArgs e) {
            var textBox = (TextBox)sender;
            int id = 0;
            if (int.TryParse(textBox.Text, out id)) {
                Settings.Default.SteamAppId = id;
            }
        }

        private void tbProjectName_TextChanged(object sender, EventArgs e) {
            var textBox = (TextBox)sender;
            Settings.Default.ProjectName = textBox.Text;
        }

        private void CheckCheckBoxes() {
            //cbWindows64.Enabled = cbWindows86.Enabled = !cbWindowsUniversal.Checked;
            cbWindowsRestructure.Enabled = cbWindows64.Checked && cbWindows86.Checked;

            cbLinux64.Enabled = cbLinux86.Enabled = !cbLinuxUniversal.Checked;
            //cbLinuxRestructure.Enabled = cbLinux64.Checked && cbLinux86.Checked;

            cbMac64.Enabled = cbMac86.Enabled = !cbMacUniversal.Checked;
            //cbMacRestructure.Enabled = cbMac64.Checked && cbMac86.Checked;
        }

        private async void bStart_Click(object sender, EventArgs e) {
            //TODO create normal debug messages
            await DebugLog("Start building...");

            var processes = Process.GetProcessesByName(Resources.UnityEditorProcessName);
            while (processes.Length > 0) {
                //TODO create normal messages
                var dialogResult =
                    MessageBox.Show(
                        "Please close the Unity Editor, in order to avoid errors.",
                        "Detected process \"Unity Editor\"", MessageBoxButtons.AbortRetryIgnore);
                switch (dialogResult) {
                    case DialogResult.Abort:
                        //TODO create normal debug messages
                        await DebugLog("Abort building.");
                        return;
                    case DialogResult.Retry:
                        processes = Process.GetProcessesByName(Resources.UnityEditorProcessName);
                        break;
                    case DialogResult.Ignore:
                        goto IgnoreUnityEditorProcess;
                }
            }

            IgnoreUnityEditorProcess:
            await Builder.StartBuild(DebugLog);
            //TODO create normal debug messages
            await DebugLog("Finish building.");
            //TODO create normal messages
            MessageBox.Show(@"Done");
        }

        public delegate Task DebugLogDelegate(string text);

        private async Task DebugLog(string text) {
            tbDebugLog.AppendText(text + "\r\n");
        }
    }
}
