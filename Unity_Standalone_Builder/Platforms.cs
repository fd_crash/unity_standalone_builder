﻿
namespace Unity_Standalone_Builder {
    internal static class Platforms {
        internal const string Windows = "Windows";
        internal const string Linux = "Linux";
        internal const string MacOs = "Mac";
    }
}
